
// npm i express dotenv ioredis

import express from 'express'
import { sendMessageToRedis } from './controller.js'
import RedisConfig from './config.js'

const app = express()

app.use(express.json())

app.post('/api/send', sendMessageToRedis)

const redisconfig = new RedisConfig()
redisconfig.consume('create:blog', (message) => {
    console.log('Received message: ', message)
})

app.listen(process.env.PORT)

